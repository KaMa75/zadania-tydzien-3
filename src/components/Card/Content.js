import React from 'react';

function Content({children}) {
    return (
        <p className="Content">
            {children}
        </p>
    );
}

export default Content;
