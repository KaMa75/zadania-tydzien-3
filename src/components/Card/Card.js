import React from 'react';

import {Title, Intro, Content, Image, ShowBtn} from './';
import './Card.css';

function Card({title, intro, content, showMore}) {
    const contentElement = showMore && <Content>{content}</Content>;
    return (
        <div className="Card">
            <Title letter="R">
                {title}
            </Title>
            <Image />
            <Intro>
                {intro}
            </Intro>
            <ShowBtn />
            {contentElement}
        </div>
    );
}

export default Card;
