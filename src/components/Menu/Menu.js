import React from 'react';

function Menu({children}) {
    return (
        <nav>
            <ul>
                {children}
            </ul>
        </nav>
    );
}

export default Menu;
